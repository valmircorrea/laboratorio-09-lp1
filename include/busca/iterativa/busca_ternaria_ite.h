/**
* @file     busca_ternaria_ite.h
* @brief    Arquivo com a função de busca ternaria na forma iterativa.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since    19/06/17
* @date     20/06/17
*/

#ifndef BUSCA_TERNARIA_ITE_H_
#define BUSCA_TERNARIA_ITE_H_

namespace edb1 {

    /**
    * @brief Realiza a busca ternaria de elemento no vetor.
    * @param *v Ponteiro genérico que aponta para um vetor.
    * @param ini Recebe o índice do inicio do vetor.
    * @param fim Recebe o índice do fim do vetor.
    * @param chave Recebe o elemento a ser buscado no vetor.
    * @return Retorna true ou false, se o elemento estiver no vetor ou não.
    */
    extern "C++" template <typename T>
    bool BuscaTernaria_ite (T *v, int ini, int fim, T chave) {

        if (fim <= ini) {
            return false;
        }

        do {

            int mid1 = ini + ((fim - ini)/3);
            int mid2 = ini + (2 *(fim - ini)/3);

            if (chave == v[mid1] ) {
                return true;
            }
            else if (chave == v[mid2]) {
                return true;
            }
            else if (chave < v[mid1]) {
                fim = mid2 - 1;
            }
            else if (chave > v[mid1] && chave < v[mid2] ) {
                ini = mid1 + 1;
                fim = mid2 - 1;
            }
            else if (chave > v[mid2]) {
                ini = mid2 + 1;
            }

        } while (ini <= fim);

        return false;
    }
}

#endif