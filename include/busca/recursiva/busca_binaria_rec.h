/**
* @file     busca_binaria_rec.h
* @brief    Arquivo com a função de busca binária.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since    19/06/17
* @date     20/06/17
*/

#ifndef BUSCA_BINARIA_REC_H_
#define BUSCA_BINARIA_REC_H_

namespace edb1 {

    /**
    * @brief Função que realiza a busca binaria em um vetor.
    * @param *v Ponteiro genérico que aponta para um vetor.
    * @param N Variável que define o tamanho do vetor.
    * @param chave Variável que recebe a chave de busca a ser encontrada.
    * @return Retorna true ou false, se o elemento estiver no vetor ou não.
    */
    extern "C++" template <typename T>
    bool BuscaBinaria_rec (T *v, int N, T chave) {

        if (N <= 0) {
            return N;
        }

        int k = N/2;

        if (v[k] == chave) {
            return true;
        }
        else if (v[k] > chave) {
            return BuscaBinaria_rec (v, k, chave); 
        }
        else if (v[k] < chave) {
            return BuscaBinaria_rec (&v[k+1], N - k - 1, chave); 
        }

        return false;
    }
}

#endif