/**
* @file	    valmir.h
* @brief	Arquivo com biblioteca 'valmir'.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since	20/06/2017
* @date     20/06/2017
*/

#ifndef VALMIR_H
#define VALMIR_H

#include "busca_binaria_ite.h"              /**< Arquivo com a busca binária na forma iterativa */
#include "busca_sequencial_ite.h"           /**< Arquivo com a busca sequêncial na forma iterativa */
#include "busca_ternaria_ite.h"             /**< Arquivo com a busca ternária na forma iterativa */

#include "busca_binaria_rec.h"              /**< Arquivo com a busca binária na forma recursiva */
#include "busca_sequencial_rec.h"           /**< Arquivo com a busca sequêncial na forma recursiva */
#include "busca_ternaria_rec.h"             /**< Arquivo com a busca ternária na forma recursiva */

#include "bubble_sort.h"                    /**< Arquivo com o método de ordenação bubble sort */
#include "insertion_sort.h"                    /**< Arquivo com o método de ordenação insert sort */
#include "merge_sort.h"                     /**< Arquivo com o método de ordenação merge sort */
#include "quick_sort.h"                     /**< Arquivo com o método de ordenação quick sort */
#include "selection_sort.h"                 /**< Arquivo com o método de ordenação selection sort */

#include "fila.h"                           /**< Arquivo com classe fila */
#include "list_dup_enc_sent.h"              /**< Arquivo com classe lista duplamente ligada */
#include "stack.h"                          /**< Arquivo com classe pilha */

namespace edb1 {

}

#endif