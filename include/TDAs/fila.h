/**
* @file	    fila.h
* @brief	Arquivo com a classe fila, seus atributos e métodos.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since	20/06/2017
* @date     20/06/2017
*/

#ifndef FILA_H
#define FILA_H

namespace edb1 {

    /**
    * @class   Fila
    * @brief   Classe que representa uma fila.
    */
    template<typename T>
    class Fila {
        private:

            int qtd_itens;                      /**< Quantidade de itens que há na fila*/
            int capacidade;                     /**< Capacidade de itens que a fila comporta */
            T *dados;                           /**< Ponteiro para o tipo de dados da fila */

        public:
            
            Fila (int tam);                     /**< Construtor parametrizado */

            void push (T val);                  /**< Insere um dado na fila */
            T pop ();                           /**< Retira um dado da fila */
            bool full ();                       /**< Verifica se a fila está cheia */
            bool empty ();                      /**< Verifica se a fila está vazia */

            ~Fila ();                           /**< Destrutor padrão */
    };

    /**
    * @brief Construtor parametrizado.
    * @param t Variável que recebe o tamanho da fila.
    */
    template<typename T>
    Fila<T>::Fila (int t) {

        dados = new T[t];
        capacidade = t;
    }

    /**
    * @brief Método que insere um item na fila.
    * @param i Variável que recebe o item a ser inserido.
    */
    template<typename T>
    void Fila<T>::push (T i) {

        if( !full() ) {
            dados[qtd_itens++] = i;
        }
    }

    /**
    * @brief Método que retira um item da fila.
    */
    template<typename T>
    T Fila<T>::pop () {

        if( !empty() ) {

            qtd_itens--;
            T temp = dados[0];

            for(int ii = 0; ii < qtd_itens; ii++) {
                dados[ii] = dados[ii + 1];
            }
                
            return temp;
        } 
        else {
            return dados[0];
        }
    }

    /**
    * @brief Método que verifica se a fila está cheia.
    * @return Retorna 'true' ou 'false', se a fila está cheia ou não, respectivamente.
    */
    template<typename T>
    bool Fila<T>::full () {

        if (qtd_itens == capacidade) {
            return true;
        }

        return false;
    }

    /**
    * @brief Método que verifica se a fila está vazia.
    * @return Retorna 'true' ou 'false', se a fila está vazia ou não, respectivamente.
    */
    template<typename T>
    bool Fila<T>::empty () {

        if (qtd_itens == 0) {
            return true;
        }

        return false;
    }

    /**
    * @brief Destrutor padrão para liberar o espaço na memória.
    */
    template<typename T>
    Fila<T>::~Fila () {
        delete[] dados;
    }
}

#endif