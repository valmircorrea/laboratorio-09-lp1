/**
* @file     merge_sort.h
* @brief    Arquivo com a função de ordenação insert sort.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since    19/06/17
* @date     20/06/17
*/

#ifndef MERGE_SORT_H_
#define MERGE_SORT_H_

namespace edb1 {

    /**
    * @brief Função que realiza a ordenação de um vetor.
    * @param *v Ponteiro genérico que aponta para um vetor.
    * @param *aux Ponteiro genérico que aponta para um vetor auxiliar.
    * @param ini Variável que recebe o valor do inicio do vetor.
    * @param pivo Variável que recebe a posição media do vetor.
    * @param fim Variável que recebe o valor do final do vetor.
    */
    template <typename T>
    void merge (T *v, T *aux, int ini, int pivo, int fim) {

        int hh = ini;
        int ii = ini;
        int kk = 0;
        int jj = pivo + 1;

        while ( (hh <= pivo) && (jj <= fim) ) {

            if (v[hh] <= v[jj]) {
                aux[ii] = v[hh];
                hh++;
            }
            else {

                aux[ii] = v[jj];
                jj++; 
            }
            
            ii++;
        }
        if (hh > pivo) {

            for ( kk = jj; kk <= fim; kk++) {

                aux[ii] = v[kk];
                ii++;
            }
        }
        else {
            for (kk = hh; kk <= pivo; kk++) {

                aux[ii] = v[kk];
                ii++;
            }
        }
        for (kk = ini; kk <= fim; kk++) {
            v[kk] = aux[kk];
        } 
    }

    /**
    * @brief Função que realiza a ordenação de um vetor.
    * @param *v Ponteiro genérico que aponta para um vetor.
    * @param ini Variável que recebe o valor do inicio do vetor.
    * @param fim Variável que recebe o valor do final do vetor.
    */
    extern "C++" template <typename T>
    void MergeSort (T *v, int ini, int fim) {

        T *aux = new T [ini + fim];

        if (ini < fim) {

            int pivo = (ini + fim)/2;

            MergeSort (v, ini, pivo);
            MergeSort (v, pivo + 1, fim);
            merge (v, aux, ini, pivo, fim);
        }

        delete [] aux;
    }
}

#endif