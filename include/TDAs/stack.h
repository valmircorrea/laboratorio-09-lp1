/**
* @file	    stack.h
* @brief	Arquivo com a classe stack, seus atributos e métodos.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since	20/06/2017
* @date     20/06/2017
*/

#ifndef STACK_H
#define STACK_H

#include <iostream>
using std:: cout;
using std:: endl;

namespace edb1 {

    /**
    * @class   Stack 
    * @brief   Classe que representa uma pilha.
    */
    template <typename T>
    class Stack {
        private:

        T *vector;                  /** < Vetor */
        int n_elements;             /** < Numero de elementos */
        int size_max;               /** < Tamanho máximo */ 

        public:

        Stack(int n);               /** < Construtor padrão */

        void push (T element);      /** < Insere no topo */
        void pop ();                /** < Remove um elemento do topo */
        T top ();                   /** < Retorna o ultimo elemento da pilha */
        bool full ();               /** < Vetifica se está cheio */
        bool empty ();                      /**< Verifica se a fila está vazia */

        ~Stack();                   /** < Destrutor padrão */
    };

    /**
    * @brief Contrutor parametrizado.
    */
    template <typename T>
    Stack<T>::Stack (int n) {

        vector = new T [n];
        n_elements = 0;
        size_max = n;
    }

    /**
    * @brief  Método que coloca um elemento na pilha.
    * @param  element Variavél que recebe o elemento a ser colocado na pilha.
    */
    template <typename T>
    void Stack<T>::push (T element) {

        if ( !full () ) {

            vector[n_elements] = element;
            n_elements++;
        }

        else {
            cout << "Stack is full!" << endl;
        }
    }

    /**
    * @brief  Método que tira um elemento no final da pilha.
    */
    template <typename T>
    void Stack<T>::pop () {

        n_elements -= 1;
    }

    /**
    * @brief Método que acessa o ultimo elemento da pilha.
    * @return Retorna o ultimo elemento da pilha.
    */
    template <typename T>
    T Stack<T>::top () {

        return vector[n_elements-1];
        
    }

    /**
    * @brief Método que verifica se a pilha esta cheia.
    * @return Retorna 'true' se estiver cheio ou 'false', se não ésta cheio.
    */
    template <typename T>
    bool Stack<T>::full () {

        if (n_elements == size_max) {
            return true;
        }
        
        return false; 
    }

    /**
    * @brief Método que verifica se a fila está vazia.
    * @return Retorna 'true' ou 'false', se a fila está vazia ou não, respectivamente.
    */
    template<typename T>
    bool Stack<T>::empty () {

        if (n_elements == 0) {
            return true;
        }

        return false;
    }

    /**
    * @brief Destrutor padrão para liberar o espaço na memória.
    */
    template <typename T>
    Stack<T>::~Stack () {

        delete [] vector;
    }
}

#endif