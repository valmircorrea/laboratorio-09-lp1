#Makefile for "Laboratório 09 - LP1" C++ application.
#Created by Valmir Correa 19/06/2017.

RM = rm -rf

# Compilador:
CC = g++

.PHONY: doc doxygen

# Variaveis para os subdiretorios:
BIN_DIR = ./bin
INC_DIR = ./include
SRC_DIR = ./src
DOC_DIR = ./doc
OBJ_DIR = ./build
LIB_DIR = ./lib

# Opcoes de compilacao:
CFLAGS = -Wall -pedantic -ansi -std=c++11 -I. -I$(INC_DIR)

CFLAGS += -I. -I$(INC_DIR)/busca/iterativa
CFLAGS += -I. -I$(INC_DIR)/busca/recursiva
CFLAGS += -I. -I$(INC_DIR)/ordenacao
CFLAGS += -I. -I$(INC_DIR)/TDAs

ARCHIVE = ar

linux: valmir.a valmir.so prog_estatico prog_dinamico

windows: valmir.lib valmir.dll prog_estatico.exe prog_dinamico.exe

#LINUX
valmir.a: $(SRC_DIR)/valmir.cpp $(INC_DIR)/valmir.h $(INC_DIR)/busca/iterativa/busca_binaria_ite.h $(INC_DIR)/busca/iterativa/busca_sequencial_ite.h $(INC_DIR)/busca/iterativa/busca_ternaria_ite.h $(INC_DIR)/busca/recursiva/busca_binaria_rec.h $(INC_DIR)/busca/recursiva/busca_sequencial_rec.h $(INC_DIR)/busca/recursiva/busca_ternaria_rec.h $(INC_DIR)/ordenacao/bubble_sort.h $(INC_DIR)/ordenacao/insertion_sort.h $(INC_DIR)/ordenacao/merge_sort.h $(INC_DIR)/ordenacao/quick_sort.h $(INC_DIR)/ordenacao/selection_sort.h $(INC_DIR)/TDAs/fila.h $(INC_DIR)/TDAs/list_dup_enc_sent.h $(INC_DIR)/TDAs/node.h $(INC_DIR)/TDAs/stack.h
	$(CC) $(CFLAGS) -c $(SRC_DIR)/valmir.cpp -o $(OBJ_DIR)/valmir.o
	$(AR) rcs $(LIB_DIR)/$@ $(OBJ_DIR)/valmir.o
	@echo "+++ [Biblioteca estatica criada em $(LIB_DIR)/$@] +++"

valmir.so: $(SRC_DIR)/valmir.cpp $(INC_DIR)/valmir.h $(INC_DIR)/busca/iterativa/busca_binaria_ite.h $(INC_DIR)/busca/iterativa/busca_sequencial_ite.h $(INC_DIR)/busca/iterativa/busca_ternaria_ite.h $(INC_DIR)/busca/recursiva/busca_binaria_rec.h $(INC_DIR)/busca/recursiva/busca_sequencial_rec.h $(INC_DIR)/busca/recursiva/busca_ternaria_rec.h $(INC_DIR)/ordenacao/bubble_sort.h $(INC_DIR)/ordenacao/insertion_sort.h $(INC_DIR)/ordenacao/merge_sort.h $(INC_DIR)/ordenacao/quick_sort.h $(INC_DIR)/ordenacao/selection_sort.h $(INC_DIR)/TDAs/fila.h $(INC_DIR)/TDAs/list_dup_enc_sent.h $(INC_DIR)/TDAs/node.h $(INC_DIR)/TDAs/stack.h
	$(CC) $(CFLAGS) -fPIC -c $(SRC_DIR)/valmir.cpp -o $(OBJ_DIR)/valmir.o
	$(CC) -shared -fPIC -o $(LIB_DIR)/$@ $(OBJ_DIR)/valmir.o
	@echo "+++ [Biblioteca dinamica criada em $(LIB_DIR)/$@] +++"

prog_estatico:
	$(CC) $(CFLAGS) $(SRC_DIR)/main.cpp $(LIB_DIR)/valmir.a -o $(BIN_DIR)/$@

prog_dinamico:
	$(CC) $(CFLAGS) $(SRC_DIR)/main.cpp $(LIB_DIR)/valmir.so -o $(BIN_DIR)/$@


#WINDOWS
valmir.lib: $(SRC_DIR)/valmir.cpp $(INC_DIR)/valmir.h $(INC_DIR)/busca/iterativa/busca_binaria_ite.h $(INC_DIR)/busca/iterativa/busca_sequencial_ite.h $(INC_DIR)/busca/iterativa/busca_ternaria_ite.h $(INC_DIR)/busca/recursiva/busca_binaria_rec.h $(INC_DIR)/busca/recursiva/busca_sequencial_rec.h $(INC_DIR)/busca/recursiva/busca_ternaria_rec.h $(INC_DIR)/ordenacao/bubble_sort.h $(INC_DIR)/ordenacao/insertion_sort.h $(INC_DIR)/ordenacao/merge_sort.h $(INC_DIR)/ordenacao/quick_sort.h $(INC_DIR)/ordenacao/selection_sort.h $(INC_DIR)/TDAs/fila.h $(INC_DIR)/TDAs/list_dup_enc_sent.h $(INC_DIR)/TDAs/node.h $(INC_DIR)/TDAs/stack.h
	$(AR) rcs $(LIB_DIR)/$@ $(OBJ_DIR)/valmir.o
	@echo "+++ [Biblioteca estatica criada em $(LIB_DIR)/$@] +++"

valmir.dll: $(SRC_DIR)/valmir.cpp $(INC_DIR)/valmir.h $(INC_DIR)/busca/iterativa/busca_binaria_ite.h $(INC_DIR)/busca/iterativa/busca_sequencial_ite.h $(INC_DIR)/busca/iterativa/busca_ternaria_ite.h $(INC_DIR)/busca/recursiva/busca_binaria_rec.h $(INC_DIR)/busca/recursiva/busca_sequencial_rec.h $(INC_DIR)/busca/recursiva/busca_ternaria_rec.h $(INC_DIR)/ordenacao/bubble_sort.h $(INC_DIR)/ordenacao/insertion_sort.h $(INC_DIR)/ordenacao/merge_sort.h $(INC_DIR)/ordenacao/quick_sort.h $(INC_DIR)/ordenacao/selection_sort.h $(INC_DIR)/TDAs/fila.h $(INC_DIR)/TDAs/list_dup_enc_sent.h $(INC_DIR)/TDAs/node.h $(INC_DIR)/TDAs/stack.h
	$(CC) -shared -o $(LIB_DIR)/$@ $(OBJ_DIR)/valmir.o
	@echo "+++ [Biblioteca dinamica criada em $(LIB_DIR)/$@] +++"

prog_estatico.exe:
	$(CC) $(CFLAGS) $(SRC_DIR)/main.cpp $(LIB_DIR)/valmir.lib -o $(BIN_DIR)/$@

prog_dinamico.exe:
	$(CC) $(CFLAGS) $(SRC_DIR)/main.cpp $(LIB_DIR)/valmir.dll -o $(BIN_DIR)/$@


# Alvo para a geração automatica de documentação usando o Doxygen:
doxygen:
	doxygen -g

doc:
	@echo "====================================================="
	@echo "Limpando pasta $(DOC_DIR)"
	@echo "====================================================="
	$(RM) $(DOC_DIR)/*
	@echo "====================================================="
	@echo "Gerando nova documentação na pasta $(DOC_DIR)"
	@echo "====================================================="
	doxygen Doxyfile


# Alvo usado para limpar os arquivos temporários:
clean:
	@echo "Removendo arquivos objeto e executaveis/binarios..."
	@rm -rf $(BIN_DIR)/*
	@rm -rf $(OBJ_DIR)/*
